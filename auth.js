var router = require('express').Router();
var jwt = require('jwt-simple');
var passport = require('passport');
var passportJwt = require('passport-jwt');
var express = require('express');
var mongoose = require('mongoose');
var user = require('./user');
var config = require('./config');

exports.strategy = function (passport) {
    var opts = {};
    opts.secretOrKey = config.secret;
    opts.jwtFromRequest = passportJwt.ExtractJwt.fromAuthHeader();
    passport.use(new passportJwt.Strategy(opts, function (jwt_payload, done) {
        user.User.findOne({ id: jwt_payload.id }, function (error, user) {
            if (error)
                return done(error, false);
            if (user)
                done(null, user);
            else
                done(null, false);
        });
    }));
};

router.post('/register', function (request, response, next) {
    var newUser = new user.User({
        name: request.body.name,
        password: request.body.password
    });

    newUser.save(function (error) {
        if (error) {
            return response.json({ 
                    success: false,
                    message: 'Username already exists'
            });
        }

        response.json({
            success: true,
            message: 'User created'
        });
    });
});

router.post('/authenticate', function (request, response, next) {
    user.User.findOne({
            name: request.body.name
    }, function (error, users) {
        if (error)
            throw error;

        if (!users)
            return response.json({ success: false, message: "Authentication failed. User not found" });

        users.comparePassword(request.body.password, function (error, isMatch) {
            if (error)
                throw error;

            if (isMatch) {
                var token = jwt.encode(users, config.secret);
                return response.json({ success: true, token: 'JWT ' + token });
            } 

            return response.send({ success: false, message: 'Authentication failed. Wrong password.' });
        });
    });
});

router.get('/account', passport.authenticate('jwt', { session: false }), function (request, response) {
    var token = getToken(request.headers);
    if (!token) 
        return response.status(403).send({ success: false, message: "No token provided" });

    var decoded = jwt.decode(token, config.secret);
    user.User.findOne({
            name: decoded.name
    }, function (error, users) {
        if (error)
            throw error;

        if (!users)
            return response.status(403).send({ success: false, message: 'Authentication failed. User not found' });
        else 
            response.json(users);
    });
});

var getToken = function (headers) {
    if (headers && headers.authorization) {
        var parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;        
    }
}

exports.router = router;
