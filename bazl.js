"use strict";
var mongoose = require('mongoose');

var categories = [
    'Books and Stationary',
    'Cars and Motors',
    'Electronics',
    'Entertainment',
    'Fashion and Accessories',
    'Furniture',
    'Home and Garden',
    'Services',
    'Sports and Leisure'
];

var BazlSchema = new mongoose.Schema({
    acceptsTrades: {
        type: Boolean,
        default: false
    },
    acceptsOffers: {
        type: Boolean,
        default: false
    },
    category: {
        type: String,
        enum: categories,
        required: true
    },
    coordinates: {
        type: [Number],
        index: '2dsphere'
    },
    dateCreated: {
        type: Date,
        default: Date.now
    },
    description: String,
    images: [String],
    price: {
        type: Number,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    views: {
	type: Number,
	default: 0,
	min: 0
    },
    stars: {
	type: Number,
	default: 0,
	min: 0	    
    }
});

exports.categories = categories;
exports.Bazl = mongoose.model('Bazl', BazlSchema);
