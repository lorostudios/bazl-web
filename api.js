"use strict";
var express = require('express');
var bazl = require('./bazl');
var router = express.Router();

router.route('/bazl/new')
    .post(function (req, res, next) {
    var newBazl  = new bazl.Bazl(req.body);
    newBazl.save(function (error, bazls) {
        if (error)
            return next(error);
        res.sendStatus(201);
    });
});

router.route('/bazl/categories')
    .get(function (req, res, next) {
    res.json({
        'categories': bazl.categories
    });
});

router.route('/bazl/search/')
    .get(function (req, res, next) {
    var maxDistance = 8046; // meters
    var defaultQuery = { $exists: true };
    if (req.query.longitude && req.query.latitude) {
        bazl.Bazl.geoNear({
            type: 'Point',
            coordinates: [parseFloat(req.query.longitude), parseFloat(req.query.latitude)]
        }, {
            maxDistance: req.query.maxDistance ? req.query.maxDistance : maxDistance,
            spherical: true,
            query: {
                acceptsTrades: req.query.acceptsTrades ? req.query.acceptsTrades : defaultQuery,
                acceptsOffers: req.query.acceptsOffers ? req.query.acceptsOffers : defaultQuery,
                category: req.query.category ? req.query.category : defaultQuery,
                condition: req.query.condition ? { $gte: req.query.condition } : defaultQuery,
                price: req.query.price ? { $lte: req.query.price } : defaultQuery
            }
        }, function (error, bazls) {
            if (error)
                return next(error);
            res.json(bazls);
        });
    }
    else {
        bazl.Bazl.find({
            acceptsTrades: req.query.acceptsTrades ? req.query.acceptsTrades : defaultQuery,
            acceptsOffers: req.query.acceptsOffers ? req.query.acceptsOffers : defaultQuery,
            category: req.query.category ? req.query.category : defaultQuery,
            condition: req.query.condition ? { $gte: req.query.condition } : defaultQuery,
            price: req.query.price ? { $lte: req.query.price } : defaultQuery
        }, function (error, bazls) {
            if (error)
                return next(error);
            res.json(bazls);
        });
    }
});

router.route('/bazl/view/').put(function (req, res, next) {
	bazl.Bazl.findByIdAndUpdate(req.query.id, {
		$inc: {
			views: 1
		}
	}, {
		new: true
	}, function (error, bazls) {
		if (error)
			return next(error);
		res.json(bazls);
	});
});

router.put('/bazl/star/', function (req, res, next) {
	bazl.Bazl.findByIdAndUpdate(req.query.id, {
		$inc: {
			stars: 1
		}
	}, {
		new: true
	}, function (error, bazls) {
		if (error)
			return next(error);
		res.json(bazls);
	});
});

router.put('/bazl/unstar/', function (req, res, next) {
	bazl.Bazl.findByIdAndUpdate(req.query.id, {
		$inc: {
			stars: -1
		}
	}, {
		new: true
	}, function (error, bazls) {
		if (error)
			return next(error);
		res.json(bazls);
	});
});

exports.router = router;
