var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var config = require('./config');

var UserSchema = new mongoose.Schema({
        name: {
            type: String,
            unique: true,
            required: true
        },
        password: {
            type: String,
            required: true
        }
});

UserSchema.pre('save', function(next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (error, salt) {
            if (error)
                return next(error);

            bcrypt.hash(user.password, salt, function(error, hash) {
                if (error)
                    return next(error);

                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function (password, cb) {
    bcrypt.compare(password, this.password, function (error, isMatch) {
        if (error)
            return next(error);

        cb(null, isMatch);
    });
};

exports.User = mongoose.model('User', UserSchema);
